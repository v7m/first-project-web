require 'acceptance_helper'
require 'faker'

resource "Authorizations", acceptance: :request do

  post '/api/v1/auth/sign_up' do
    let(:user) { create(:user) }

    with_options scope: :user, required: true do
      parameter :email, 'Email', required: true
      parameter :password, 'Password', required: true
      parameter :password_confirmation, 'Password confirmation', required: true
      parameter :username, 'Username', required: true
    end

    with_options scope: :session, required: true do
      parameter :device_platform, 'Device platform', required: true
      parameter :device_token, 'Device token', required: true
    end

    example 'Sign up (valid params)' do
      params = {
          user: {
              email: 'valid_email@example.com',
              password: '12345678',
              password_confirmation: '12345678',
              username: 'username1'
          },
          session: {
              device_platform: 'ios',
              device_token: 'device_token'
          }
      }

      do_request(params)

      created_user = User.find_by_email('valid_email@example.com')

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(response_headers['Access-Token']).to eq created_user.sessions.first.access_token
      expect(body['id']).not_to eq nil
      expect(body['email']).to eq created_user.email
      expect(body['username']).to eq created_user.username
      expect(body['avatar']).to eq created_user.avatar.url
    end

    example 'Sign up (email has already been taken)' do
      params = {
          user: {
              email: user.email,
              password: '12345678',
              password_confirmation: '12345678',
              username: 'username1'
          },
          session: {
              device_platform: 'ios',
              device_token: 'device_token'
          }
      }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 422
      expect(body['message']).to eq 'Email has already been taken'
    end

    example 'Sign up (password confirmation doesn\'t match password)' do
      params = {
          user: {
              email: 'valid_email@example.com',
              password: '12345678',
              password_confirmation: '123456780',
              username: 'username1'
          },
          session: {
              device_platform: 'ios',
              device_token: 'device_token'
          }
      }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 422
      expect(body['message']).to eq 'Password confirmation doesn\'t match Password'
    end

    example 'Sign up (password is too short)' do
      params = {
          user: {
              email: 'valid_email@example.com',
              password: '123',
              password_confirmation: '123',
              username: 'username1'
          },
          session: {
              device_platform: 'ios',
              device_token: 'device_token'
          }
      }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 422
      expect(body['message']).to eq 'Password is too short (minimum is 6 characters)'
    end
  end

  post '/api/v1/auth/login' do
    let(:user) { create(:user) }

    with_options scope: :user, required: true do
      parameter :email, 'Email', required: true
      parameter :password, 'Password', required: true
    end

    with_options scope: :session, required: true do
      parameter :device_platform, 'Device platform', required: true
      parameter :device_token, 'Device token', required: true
    end

    example 'Login (valid params)' do
      params = {
          user: {
              email: user.email,
              password: '12345678'
          },
          session: {
              device_platform: 'ios',
              device_token: 'device_token'
          }
      }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(response_headers['Access-Token']).to eq user.sessions.first.access_token

      expect(body['id']).to eq user.id
      expect(body['email']).to eq user.email
      expect(body['username']).to eq user.username
      expect(body['avatar']).to eq user.avatar.url
    end

    example 'Login (invalid email)' do
      params = {
          user: {
              email: 'wrong_email@example.com',
              password: '12345678'
          },
          session: {
              device_platform: 'ios',
              device_token: 'device_token'
          }
      }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 400
      expect(body['message']).to eq 'Invalid email or password'
    end

    example 'Login (invalid password)' do
      params = {
          user: {
              email: user.email,
              password: 'wrong_password'
          },
          session: {
              device_platform: 'ios',
              device_token: 'device_token'
          }
      }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 400
      expect(body['message']).to eq 'Invalid email or password'
    end
  end
end
