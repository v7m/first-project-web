require "acceptance_helper"

resource "Deposit", acceptance: :request do
  let(:user) { create(:user, :with_ios_session) }
  let(:session) { user.sessions.first }

  before do
    header 'Access-Token', session.access_token
  end

  get '/api/v1/deposits/new' do
    example 'Get Braintree client token' do
      do_request

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['client_token']).to_not eq nil
    end
  end

  post '/api/v1/deposits' do
    with_options scope: :deposits, required: true do
      parameter :amount, 'Amount', required: true
    end
    parameter :payment_method_nonce, 'Payment method nonce', required: true

    example 'Create deposit (valid params)' do
      params = {
          deposits: {
              amount: 10.0,
          },
          payment_method_nonce: 'fake-valid-nonce'
      }

      do_request(params)

      body = JSON.parse(response_body)

      deposit = user.financial_profile.deposits.first

      expect(status).to eq 200
      expect(body['message']).to eq 'Success!'
      expect(deposit.amount).to eq 10.0
      expect(deposit.transaction_id).to_not eq nil
      # expect(deposit.financial_profile.amount).to eq 10.0
    end

    example 'Create deposit (invalid params)' do
      params = {
          deposits: {
              amount: 10.0
          }
      }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 400
      expect(body['message']).to eq 'Something went wrong while processing your transaction. Please try again!'
    end
  end
end