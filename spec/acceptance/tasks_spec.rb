require 'acceptance_helper'
require 'faker'

resource "Tasks", acceptance: :request do
  let(:first_user) { create(:user, :with_ios_session) }
  let(:second_user) { create(:user, :with_ios_session) }
  let(:doer) { create(:user, :with_ios_session) }
  let(:first_user_session) { first_user.sessions.first }
  let(:second_user_session) { second_user.sessions.first }
  let(:doer_session) { doer.sessions.first }
  let!(:task_without_video) { create(:task, :not_approved, creator: first_user, doer: doer) }
  let!(:task_with_video) { create(:task, :approved, :with_video, creator: first_user, title: 'Approved') }
  let!(:closed_task) { create(:task, :closed, creator: first_user, desired_rate: 1.0) }

  before { header 'Access-Token', first_user_session.access_token }

  get '/api/v1/tasks/approved' do
    with_options scope: :filter do
      parameter :title, 'Title'
      parameter :desired_rate, 'Desired rate'
      parameter :created_at, 'Created at'
    end
    parameter :page, 'Page'

    example 'Get approved tasks' do
      params = {
        page: 1,
        filter: {
          title: 'Approved'
        }
      }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['data'].count).to eq 1
    end
  end

  get '/api/v1/tasks/closed' do
    with_options scope: :filter do
      parameter :title, 'Title'
      parameter :desired_rate, 'Desired rate'
      parameter :created_at, 'Created at'
    end
    parameter :page, 'Page'

    example 'Get closed tasks' do
      params = {
        page: 1,
        filter: {
          desired_rate: 1.0
        }
      }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['data'].count).to eq 1
    end
  end

  get '/api/v1/tasks/:id' do
    let(:id) { task_with_video.id }
    let!(:comment) { create(:comment, user: first_user, task: task_with_video) }

    example 'Get task' do
      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['id']).to eq task_with_video.id
      expect(body['title']).to eq task_with_video.title
      expect(body['state']).to eq task_with_video.state
      expect(body['category']).to eq task_with_video.category
      expect(body['description']).to eq task_with_video.description
      expect(body['items']).to eq ['first', 'second']
      expect(body['location']['latitude']).to eq 50.475069
      expect(body['location']['longitude']).to eq 30.432441
      expect(body['user_id']).to eq first_user.id
      expect(body['video_url']).to eq task_with_video.video_url
      expect(body['audio_url']).to eq nil
      expect(body['accomplish_video_url']).to eq nil
      expect(body['accomplish_audio_url']).to eq nil
      expect(body['donated_amount']).to eq Task.last.donated_amount.to_f
      expect(DateTime.parse(body['launch_at']).to_i).to eq task_with_video.launch_at.to_i
      expect(DateTime.parse(body['finish_at']).to_i).to eq task_with_video.finish_at.to_i
      expect(body['doc_proof']).to eq false
      expect(body['manage']).to eq false
      expect(body['doer_username']).to eq nil
      expect(body['doer_avatar']).to eq nil
      expect(body['creator_username']).to eq task_with_video.creator.username
      expect(body['creator_avatar']).to eq task_with_video.creator.avatar.url
      expect(body['likes']).to eq 0
      expect(body['liked_by_current_user']).to eq false
      expect(body['comments'].count).to eq 1
    end
  end

  post '/api/v1/tasks' do
    with_options scope: :task, required: true do
      parameter :title, 'Title', required: true
      parameter :description, 'Description', required: true
      parameter :category, 'Category'
      parameter :items, 'Items'
      parameter :latitude, 'Latitude'
      parameter :longitude, 'Longitude'
      parameter :video_url, 'Video url'
      parameter :image_url, 'Image url'
      parameter :desired_rate, 'Desired rate', required: true
      parameter :launch_at, 'Launch at', required: true
      parameter :finish_at, 'Finish at', required: true
      parameter :doc_proof, 'Doc proof'
      parameter :manage, 'Manage'
    end

    example 'Create task (valid params)' do
      params = {
          task: {
              title: 'Title',
              description: 'Description',
              category: 'angel',
              latitude: 50.475069,
              longitude: 30.432441,
              items: ['first', 'second'],
              desired_rate: 1000,
              launch_at: 1.hours.from_now,
              finish_at: 2.hours.from_now,
              doc_proof: true,
              manage: false
          }
      }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['id']).to eq Task.first.id
      expect(body['title']).to eq 'Title'
      expect(body['state']).to eq 'not_approved'
      expect(body['description']).to eq 'Description'
      expect(body['category']).to eq 'angel'
      expect(body['items']).to eq ['first', 'second']
      expect(body['location']['latitude']).to eq 50.475069
      expect(body['location']['longitude']).to eq 30.432441
      expect(body['user_id']).to eq first_user.id
      expect(body['video_url']).to eq nil
      expect(body['audio_url']).to eq nil
      expect(body['accomplish_video_url']).to eq nil
      expect(body['accomplish_audio_url']).to eq nil
      expect(body['desired_rate']).to eq Task.last.desired_rate.to_i
      expect(DateTime.parse(body['launch_at']).to_i).to eq 1.hours.from_now.to_i
      expect(DateTime.parse(body['finish_at']).to_i).to eq 2.hours.from_now.to_i
      expect(body['doc_proof']).to eq true
      expect(body['manage']).to eq false
      expect(body['doer_username']).to eq nil
      expect(body['doer_avatar']).to eq nil
      expect(body['creator_username']).to eq first_user.username
      expect(body['creator_avatar']).to eq first_user.avatar.url
      expect(body['likes']).to eq 0
      expect(body['liked_by_current_user']).to eq false
    end

    example 'Create task (invalid params)' do
      params = {
          task: {
              title: '',
              description: 'Description',
              desired_rate: 1000,
              launch_at: 1.hours.from_now,
              finish_at: 2.hours.from_now,
              doc_proof: true,
              manage: false
          }
      }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 422
      expect(body['message']).to eq "Title can't be blank"
    end
  end

  get '/api/v1/tasks/created' do
    with_options scope: :filter do
      parameter :title, 'Title'
      parameter :desired_rate, 'Desired rate'
      parameter :created_at, 'Created at'
    end
    parameter :page, 'Page'

    example 'Get created tasks' do
      params = { 
        page: 1,
        filter: {
          created_at: '2012-01-01'
        } 
      }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['data'].count).to eq 3
    end
  end

  get '/api/v1/tasks/doing' do
    with_options scope: :filter do
      parameter :title, 'Title'
      parameter :desired_rate, 'Desired rate'
      parameter :created_at, 'Created at'
    end
    parameter :page, 'Page'

    before { header 'Access-Token', doer_session.access_token }

    example 'Get doing tasks' do
      params = { page: 1 }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['data'].count).to eq 1
    end
  end

  get '/api/v1/tasks/in_gallery' do
    with_options scope: :filter do
      parameter :title, 'Title'
      parameter :desired_rate, 'Desired rate'
      parameter :created_at, 'Created at'
    end
    parameter :page, 'Page'

    example 'Get tasks in gallery' do
      params = {
        page: 1,
        filter: {
          title: 'Approved'
        }
      }

      task_with_video.add_to_user_gallery(first_user)

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['data'].count).to eq 1
      expect(body['data'][0]['id']).to eq task_with_video.id
    end
  end

  put '/api/v1/tasks/:id/accomplish' do
    let(:id) { task_without_video.id }

    before do
      header 'Access-Token', doer_session.access_token 
      task_without_video.approve
    end

    with_options scope: :task, required: true do
      parameter :accomplish_video_url, 'Accomplish video url', required: true
      parameter :accomplish_image_url, 'Accomplish image url', required: true
    end

    example 'Accomplish task (user is task doer, valid params)' do
      params = { task: { accomplish_video_url: 'https://example.com/video.avi' } }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['id']).to eq task_without_video.id
      expect(body['title']).to eq task_without_video.title
      expect(body['description']).to eq task_without_video.description
      expect(body['accomplish_video_url']).to eq 'https://example.com/video.avi'
    end

    example 'Accomplish task (user is task doer, invalid params)' do
      params = { task: { accomplish_video_url: 'fuck' } }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 422
      expect(body['message']).to eq "Accomplish video url is not a valid URL"
    end

    example 'Accomplish task (user isn\'t task doer)' do
      header 'Access-Token', second_user_session.access_token

      params = { task: { accomplish_video_url: 'https://example.com/video.avi' } }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 401
      expect(body['message']).to eq "User isn't task doer"
    end
  end

  put '/api/v1/tasks/:id/assign_to_me' do
    let(:id) { task_with_video.id }

    example 'Assign task to me user' do
      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['message']).to eq 'Task successfully assigned'
    end
  end

  post '/api/v1/tasks/:id/like' do
    let(:id) { task_with_video.id }

    example 'Like task' do
      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['message']).to eq 'Success!'
      expect(task_with_video.likes.count).to eq 1
    end
  end

  delete '/api/v1/tasks/:id/unlike' do
    let(:id) { task_with_video.id }

    example 'Unlike task' do
      task_with_video.like(first_user)

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['message']).to eq 'Success!'
      expect(task_with_video.likes.count).to eq 0
    end
  end

  post '/api/v1/tasks/:id/add_to_gallery' do
    let(:id) { task_with_video.id }

    example 'Add task to user gallery' do
      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['message']).to eq 'Success!'
      expect(first_user.tasks_in_gallery.include?(task_with_video)).to eq true
    end
  end

  delete '/api/v1/tasks/:id/remove_from_gallery' do
    let(:id) { task_with_video.id }

    example 'Remove task from user gallery' do
      task_with_video.add_to_user_gallery(first_user)

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['message']).to eq 'Success!'
      expect(first_user.tasks_in_gallery.count).to eq 0
    end
  end
end
