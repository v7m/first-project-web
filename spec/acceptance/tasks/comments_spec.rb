require 'acceptance_helper'
require 'faker'

resource "Tasks::Comments", acceptance: :request do
  let(:first_user) { create(:user, :with_ios_session) }
  let(:first_user_session) { first_user.sessions.first }
  let(:task_without_video) { create(:task, :approved, creator: first_user) }
  let!(:comment) { create(:comment, task: task_without_video, user: first_user) }

  before do
    header 'Access-Token', first_user_session.access_token
  end

  get '/api/v1/tasks/:task_id/comments' do
    let(:task_id) { task_without_video.id }

    example 'Get task comments' do
      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['comments'].count).to eq 1
    end
  end

  post '/api/v1/tasks/:task_id/comments' do
    let(:task_id) { task_without_video.id }

    with_options scope: :comment, required: true do
      parameter :text, 'Text', required: true
    end

    example 'Create comment (valid params)' do
      params = {
          comment: {
              text: 'Text'
          }
      }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['id']).to eq Comment.last.id
      expect(body['user_id']).to eq first_user.id
      expect(body['task_id']).to eq task_id
      expect(body['text']).to eq 'Text'
    end

    example 'Create comment (invalid params)' do
      params = { comment: { text: '' } }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 422
      expect(body['message']).to eq "Text can't be blank"
    end
  end
end
