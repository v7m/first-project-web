require "acceptance_helper"

resource "Donates", acceptance: :request do
  let(:creator) { create(:user, :with_ios_session) }
  let(:doer) { create(:user, :with_ios_session) }
  let(:donator) { create(:user, :with_ios_session) }
  let(:donator_session) { donator.sessions.first }
  let(:task) { create(:task, :approved, :with_video, creator: creator, doer: doer) }

  before do
    header 'Access-Token', donator_session.access_token
  end

  get '/api/v1/donates/new' do
    example 'Get Braintree client token' do
      do_request

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['client_token']).to_not eq nil
    end
  end

  post '/api/v1/donates/create_with_card' do
    with_options scope: :donate, required: true do
      parameter :task_id, 'Task ID', required: true
      parameter :amount, 'Amount', required: true
    end
    parameter :payment_method_nonce, 'Payment method nonce', required: true

    example 'Create donate with card (valid params)' do
      params = {
          donate: {
              task_id: task.id,
              amount: 10.0,
          },
          payment_method_nonce: 'fake-valid-nonce'
      }

      do_request(params)

      body = JSON.parse(response_body)

      donate = task.donates.first

      expect(status).to eq 200
      expect(body['message']).to eq 'Success!'
      expect(donate.task_id).to eq task.id
      expect(donate.user_id).to eq donator.id
      expect(donate.amount).to eq 10.0
      expect(donate.transaction_id).to_not eq nil
    end

    example 'Create donate with card (invalid params)' do
      params = {
          donate: {
              task_id: task.id,
              amount: 10.0
          }
      }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 400
      expect(body['message']).to eq 'Something went wrong while processing your transaction. Please try again!'
    end

  end

  post '/api/v1/donates/create_with_amount' do
    with_options scope: :donate, required: true do
      parameter :task_id, 'Task ID', required: true
      parameter :amount, 'Amount', required: true
    end

    example 'Create donate with amount (valid params)' do
      donator.financial_profile.deposit!(20.0)

      params = {
          donate: {
              task_id: task.id,
              amount: 10.0,
          }
      }

      do_request(params)

      body = JSON.parse(response_body)

      donate = task.donates.first

      expect(status).to eq 200
      expect(body['message']).to eq 'Success!'
      expect(donate.task_id).to eq task.id
      expect(donate.user_id).to eq donator.id
      expect(donate.amount).to eq 10.0
      expect(donate.transaction_id).to_not eq nil
      # expect(donator.financial_profile.amount).to eq 10.0
    end

    example 'Create donate with card (not enough money)' do

      params = {
          donate: {
              task_id: task.id,
              amount: 10.0,
          }
      }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 400
      expect(body['message']).to eq 'Not enough money!'
    end
  end
end