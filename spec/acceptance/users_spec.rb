require 'acceptance_helper'
require 'faker'

resource "Users", acceptance: :request do
  let(:first_user) { create(:user, :with_ios_session) }
  let(:second_user) { create(:user, :with_ios_session, username: 'second_user') }
  let(:first_session) { first_user.sessions.first }

  before do
    header 'Access-Token', first_session.access_token
  end

  get '/api/v1/users' do
    example 'Find users with username' do
      params = { filter_username: second_user.username }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['users'].count).to eq 1
      expect(body['users'][0]['username']).to eq second_user.username
    end
  end

  get '/api/v1/users/:id' do
    let(:id) { second_user.id }

    example 'Get another user profile' do
      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['id']).to eq second_user.id
      expect(body['email']).to eq second_user.email
      expect(body['username']).to eq second_user.username
      expect(body['avatar']).to eq second_user.avatar.url
      expect(body['status']).to eq second_user.status
      expect(body['follower']).to eq false
      expect(body['following']).to eq false
      expect(body['amount']).to eq nil
      expect(body['angel_tasks_coefficient']).to eq nil
      expect(body['angel_tasks_coefficient']).to eq nil
    end
  end

  get '/api/v1/users/followers' do
    before do
      second_user.follow(first_user)
    end

    example 'Get user followers' do
      params = { filter_username: second_user.username }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['users'].count).to eq 1
    end
  end

  get '/api/v1/users/following' do
    before do
      first_user.follow(second_user)
    end

    example 'Get user following' do
      params = { filter_username: second_user.username }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['users'].count).to eq 1
    end
  end

  get '/api/v1/users/reset_password' do
    with_options scope: :user, required: true do
      parameter :email, 'Email', required: true
    end

    before do
      header 'Access-Token', ''
    end

    example 'Reset password (valid params)' do
      params = { user: { email: first_user.email } }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['message']).to eq 'Reset password instruction has been successfully sent to user email'
    end

    example 'Reset password (invalid params)' do
      params = { user: {} }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 400
      expect(body['message']).to eq 'param is missing or the value is empty: user'
    end

    example 'Reset password (user not exist)' do
      params = { user: { email: 'wrong_email@example.com' } }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 400
      expect(body['message']).to eq 'Couldn\'t find User'
    end
  end

  get '/api/v1/users/profile' do
    example 'Get user profile' do
      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['id']).to eq first_user.id
      expect(body['email']).to eq first_user.email
      expect(body['username']).to eq first_user.username
      expect(body['avatar']).to eq first_user.avatar.url
      expect(body['status']).to eq first_user.status
      expect(body['amount']).to eq 0.0
    end
  end

  put '/api/v1/users/update_profile' do
    with_options scope: :user, required: true do
      parameter :email, 'Email'
      parameter :username, 'Username'
      parameter :avatar, 'Avatar'
      parameter :statuss, 'Status'
      parameter :position, 'Position'
    end

    example 'Update user profile (valid params)' do
      params = {
          user: {
              email: 'new_email@example.com',
              username: 'new_username',
              avatar: image_test_upload,
              status: 'Fucking status',
              position: 'Position'
          }
      }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['id']).to eq first_user.id
      expect(body['email']).to eq params[:user][:email]
      expect(body['username']).to eq params[:user][:username]
      expect(body['avatar']).to eq first_user.avatar.url
      expect(body['status']).to eq params[:user][:status]
      expect(body['position']).to eq params[:user][:position]
      expect(body['amount']).to eq 0.0
    end

    example 'Update user profile (invalid params)' do
      params = { user: { email: '' } }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 422
      expect(body['message']).to eq 'Email can\'t be blank; Email is invalid'
    end
  end

  put '/api/v1/users/update_password' do
    with_options scope: :user, required: true do
      parameter :current_password, 'Old password'
      parameter :password, 'Password'
      parameter :password_confirmation, 'Password confirmation'
    end

    example 'Update user password (valid params)' do
      new_password = 'new_password'
      params = {
          user: {
              current_password: '12345678',
              password: new_password,
              password_confirmation: new_password
          }
      }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(response_headers['Access-Token']).to eq first_session.access_token

      expect(body['message']).to eq 'Success!'
    end

    example 'Update user password (current password id wrong)' do
      new_password = 'new_password'
      params = {
          user: {
              current_password: 'wrong_password',
              password: new_password,
              password_confirmation: new_password
          }
      }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 422
      expect(response_headers['Access-Token']).to eq first_session.access_token

      expect(body['message']).to eq 'Current password is wrong'
    end

    example 'Update user password (New Password confirmation doesn\'t match new Password)' do
      params = {
          user: {
              current_password: '12345678',
              password: '11111111',
              password_confirmation: '22222222'
          }
      }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 422
      expect(response_headers['Access-Token']).to eq first_session.access_token

      expect(body['message']).to eq "New Password confirmation doesn't match new Password"
    end
  end

  post '/api/v1/users/:id/follow' do
    let(:id) { second_user.id }

    example 'Follow user (not follow yet)' do
      do_request

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['message']).to eq 'Success!'
      expect(second_user.followers.first.id).to eq first_user.id
    end

    example 'Follow user (already follow)' do
      first_user.follow(second_user)

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 400
      expect(body['message']).to eq 'Already follow'
    end
  end

  delete '/api/v1/users/:id/unfollow' do
    let(:id) { second_user.id }

    example 'Unfollow user (already follow)' do
      first_user.follow(second_user)

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['message']).to eq 'Success!'
      expect(second_user.followers.count).to eq 0
    end

    example 'Unfollow user (not follow yet)' do
      do_request

      body = JSON.parse(response_body)

      expect(status).to eq 400
      expect(body['message']).to eq 'Not follow yet'
    end
  end

  post '/api/v1/users/withdraw_money' do
    parameter :card_number, 'Card number'
    parameter :cvv, 'CVV'
    parameter :expiration_month, 'Expiration month'
    parameter :expiration_year, 'Expiration year'
    parameter :amount, required: true

    example 'Withdraw money' do
      first_user.financial_profile.deposit!(20.0)

      params = {
        card_number: '123456789',
        cvv: '123',
        expiration_month: '10',
        expiration_year: '2019',
        amount: '5.0'
      }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['message']).to eq 'Success!'
      expect(first_user.financial_profile.money_withdrawals.count).to eq 1
      # expect(first_user.financial_profile.amount).to eq 15.0
    end
  end
end