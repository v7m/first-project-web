require 'acceptance_helper'
require 'faker'

resource "Users::Tasks", acceptance: :request do
  let(:first_user) { create(:user, :with_ios_session) }
  let(:second_user) { create(:user, :with_ios_session) }
  let(:first_user_session) { first_user.sessions.first }
  let!(:second_user_created_task) { create(:task, :not_approved, creator: second_user) }
  let!(:second_user_doing_task) { create(:task, :approved, :with_video, creator: first_user, doer: second_user) }

  before { header 'Access-Token', first_user_session.access_token }

  get '/api/v1/users/:user_id/tasks/created' do
    let(:user_id) { second_user.id }

    example 'Get user created tasks' do
      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['data'].count).to eq 1
      expect(body['data'][0]['id']).to eq second_user_created_task.id
    end
  end

  get '/api/v1/users/:user_id/tasks/doing' do
    let(:user_id) { second_user.id }

    example 'Get user doing tasks' do
      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['data'].count).to eq 1
      expect(body['data'][0]['id']).to eq second_user_doing_task.id
    end
  end
end