require 'acceptance_helper'
require 'faker'

resource "Users::UnlockedUsers", acceptance: :request do
  let(:first_user) { create(:user, :with_ios_session) }
  let!(:second_user) { create(:user, :with_ios_session) }
  let(:first_session) { first_user.sessions.first }

  before do
    header 'Access-Token', first_session.access_token
  end

  delete '/api/v1/users/:user_id/unblocked_users/:id' do
    let(:user_id) { first_user.id }
    let(:id) { second_user.id }

    example 'Unblock user (already blocked)' do
      first_user.block(second_user)

      do_request

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['message']).to eq 'Success!'
      expect(first_user.blocked?(second_user)).to eq false
    end

    example 'Unblock user (not blocked yet)' do
      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 400
      expect(body['message']).to eq 'User not blocked yet'
    end
  end
end