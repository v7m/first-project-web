require 'acceptance_helper'
require 'faker'

resource "Users::BlockedUsers", acceptance: :request do
  let(:first_user) { create(:user, :with_ios_session) }
  let!(:second_user) { create(:user, :with_ios_session) }
  let(:first_session) { first_user.sessions.first }

  before do
    header 'Access-Token', first_session.access_token
  end

  post '/api/v1/users/:user_id/blocked_users' do
    let(:user_id) { first_user.id }

    parameter :blocked_user_id, 'Blocked user ID', required: true

    example 'Block user (not blocked yet)' do
      params = { blocked_user_id: second_user.id }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['message']).to eq 'Success!'
      expect(first_user.blocked?(second_user)).to eq true
    end

    example 'Block user (already blocked)' do
      first_user.block(second_user)

      params = { blocked_user_id: second_user.id }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 400
      expect(body['message']).to eq 'User already blocked'
    end
  end
end