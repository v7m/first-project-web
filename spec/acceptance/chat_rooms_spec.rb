require 'acceptance_helper'
require 'faker'

resource "Chat rooms", acceptance: :request do
  let(:first_user) { create(:user, :with_ios_session) }
  let(:second_user) { create(:user, :with_ios_session) }
  let(:first_user_session) { first_user.sessions.first }

  before do
    header 'Access-Token', first_user_session.access_token
  end

  get '/api/v1/chat_rooms/' do
    example 'Get chat rooms' do
      chat_room = ChatRoom.create_for_users(first_user, second_user)

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['chat_rooms'].count).to eq 1
      expect(body['chat_rooms'][0]['id']).to eq chat_room.id
    end
  end

  get '/api/v1/chat_rooms/get_chat_room' do
    parameter :interlocutor_id, 'Interlocutor ID', required: true

    example 'Get chat room (chat room exist)' do
      chat_room = ChatRoom.create_for_users(first_user, second_user)

      params = { interlocutor_id: second_user.id }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['id']).to eq chat_room.id
      expect(body['chanel']).to eq chat_room.chanel
      expect(body['users'].count).to eq 2
    end

    example 'Get chat room (chat room don\'t exist)' do
      params = { interlocutor_id: second_user.id }

      do_request(params)

      chat_room = ChatRoom.find_for_users(first_user, second_user)

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['id']).to eq chat_room.id
      expect(body['chanel']).to eq chat_room.chanel
      expect(body['users'].count).to eq 2
    end
  end
end

