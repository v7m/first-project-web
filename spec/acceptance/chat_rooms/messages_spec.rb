require "acceptance_helper"

resource "ChatRooms::Messages", acceptance: :request do
  let(:first_user) { create(:user, :with_ios_session) }
  let(:second_user) { create(:user, :with_ios_session) }
  let(:first_user_session) { first_user.sessions.first }
  let(:chat_room) { ChatRoom.create_for_users(first_user, second_user) }

  before do
    header 'Access-Token', first_user_session.access_token
  end

  get '/api/v1/chat_rooms/:chat_room_id/messages' do
    let(:chat_room_id) { chat_room.id }
    let!(:message) { create(:message, chat_room: chat_room, user: first_user) }

    example 'Get messages (user has chat room)' do
      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['messages'].count).to eq 1
      expect(body['messages'][0]['id'].to_i).to eq message.id
    end
  end

  post '/api/v1/chat_rooms/:chat_room_id/messages' do
    let(:chat_room_id) { chat_room.id }

    with_options scope: :message, required: true do
      parameter :body, 'Body', required: true
    end

    example 'Create message' do
      expect_any_instance_of(Pusher::Client).to receive(:trigger)

      params = {
          message: {
            body: 'some text'
          }
      }

      do_request(params)

      body = JSON.parse(response_body)

      message = Message.first

      expect(status).to eq 200
      expect(body['chat_room_id']).to eq chat_room_id
      expect(body['body']).to eq message.body
      expect(body['created_at']).not_to eq nil
      expect(body['user']['id']).to eq first_user.id
    end

    example 'Create message (user was blocked)' do
      second_user.block(first_user)

      params = {
          message: {
              body: 'some text'
          }
      }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 401
      expect(body['message']).to eq 'You were blocked'
    end
  end

end
