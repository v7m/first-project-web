require "acceptance_helper"

resource "Sessions", acceptance: :request do
  let(:user) { create(:user, :with_ios_session) }
  let(:session) { user.sessions.first }

  before do
    header 'Access-Token', session.access_token
  end

  put '/api/v1/sessions/set_push_token' do
    with_options scope: :session, required: true do
      parameter :push_token, 'Push token', required: true
    end

    example 'Set push token to current session (valid params)' do
      params = { session: { push_token: 'push_token' } }

      do_request(params)

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['message']).to eq 'Success!'
      expect(user.sessions.first.push_token).to eq 'push_token'
    end

    example 'Set push token to current session (invalid params)' do
      params = { session: {} }

      do_request(params)

      expect(status).to eq 400
    end
  end

  delete '/api/v1/sessions/destroy_session' do
    example 'Delete current session' do
      do_request

      body = JSON.parse(response_body)

      expect(status).to eq 200
      expect(body['message']).to eq 'Success!'
      expect(user.sessions.count).to eq 0
    end
  end
end