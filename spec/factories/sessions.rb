FactoryGirl.define do
  factory :session do
    access_token SecureRandom.hex
    device_token SecureRandom.hex
    push_token SecureRandom.hex

    trait :ios_platform do
      device_platform 'ios'
    end
  end
end