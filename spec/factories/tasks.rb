require 'faker'

FactoryGirl.define do
  factory :task do
    title { Faker::Lorem.word }
    description { Faker::Lorem.sentence }
    items ['first', 'second']
    latitude 50.475069
    longitude 30.432441
    desired_rate 1000
    launch_at 1.hours.from_now
    finish_at 2.hours.from_now

    trait :with_video do
      video_url 'http://example.com/video.avi'
    end

    trait :with_image do
      image_url 'http://example.com/image.png'
    end

    trait :not_approved do
      state Task.states[:not_approved]
    end

    trait :refused do
      state Task.states[:refused]
    end

    trait :approved do
      state Task.states[:approved]
    end

    trait :accomplished do
      state Task.states[:accomplished]
    end

    trait :expired do
      state Task.states[:expired]
    end

    trait :rejected do
      state Task.states[:rejected]
    end

    trait :closed do
      state Task.states[:closed]
    end

    trait :angel do
      category Task.categories[:angel]
    end

    trait :demon do
      category Task.categories[:demon]
    end
  end
end
