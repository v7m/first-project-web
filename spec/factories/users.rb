require 'faker'

FactoryGirl.define do
  factory :user do
    email { Faker::Internet.email }
    password '12345678'
    password_confirmation '12345678'
    username { Faker::Name.first_name }
    avatar { Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'support', 'images', 'test_image.png')) }
    status { Faker::Lorem.word }

    trait :with_ios_session do
      after(:create) do |student|
        create(:session, :ios_platform, user: student, device_token: SecureRandom.hex)
      end
    end
  end
end