require 'acceptance_helper'
require 'faker'

describe 'Task' do

  let(:creator) { create(:user, :with_ios_session) }
  let(:donator) { create(:user, :with_ios_session) }
  let(:task_without_video) { create(:task, :not_approved, creator: creator, doer: creator) }

  before do
    donator_financial_profile = donator.financial_profile
    donator_financial_profile.amount = 2000.0
    donator_financial_profile.save

    task_without_video.approve!
    Donates::CreateWithAmountService.new(donator, task_without_video, 1111.0).perform
    task_without_video.accomplish!
    task_without_video.close!
  end

  it 'successfully creates financial profile replenishment after task closing' do
    expect(creator.financial_profile.amount.to_f).to eq 1111.0
  end
end