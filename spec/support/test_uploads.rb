module TestUploads
  def image_test_upload
    Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'support', 'images', 'test_image.png'))
  end
end