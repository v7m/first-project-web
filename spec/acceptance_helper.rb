require 'rails_helper'
require 'rspec_api_documentation'
require 'rspec_api_documentation/dsl'

RspecApiDocumentation.configure do |config|
  config.format = :json
  config.curl_host = 'http://138.197.64.23:8080'
  config.api_name = 'Project documentation' # Your API name
  config.request_headers_to_include = ['Content-Type', 'Access-Token']
  config.response_headers_to_include = ['Content-Type', 'Access-Token']
  config.curl_headers_to_filter = ["Cookie", 'Host'] # Remove this if you want to show Auth headers in request
  config.keep_source_order = true
end
