class GalleryTask < ActiveRecord::Base
  belongs_to :user
  belongs_to :task

  validates :user, uniqueness: { scope: :task }
end
