class Session < ApplicationRecord
  TIMEOUT = Rails.application.config.timeout_session

  enum device_platform: { ios: 0 }

  belongs_to :user

  validates_presence_of :access_token, :user
  validates_uniqueness_of :access_token
  validates_inclusion_of :device_platform, in: self.device_platforms.keys
  # validates_uniqueness_of :device_token, scope: [:device_platform], allow_nil: true

  before_validation :set_access_token, on: :create

  scope :not_expired, -> {
    where('updated_at > ?', Time.current - TIMEOUT)
  }

  scope :pushable, -> {
    where(device_platform: device_platforms.values).where.not(push_token: [nil, ''])
  }

  def device_platform=(value)
    super(value) rescue super(nil)
  end

  def self.create_for(user, session_params)
    if session_params[:device_token].blank?
      session = user.sessions.build(session_params)
    else
      session = user.sessions.find_by(session_params)

      unless session
        session = Session.new(session_params)
        session.send(:set_access_token)
        session.user = user
      end
    end
    session.save
    session
  end

  protected

  def set_access_token
    begin
      self.access_token = SecureRandom.hex(20)
    end while self.class.exists?(access_token: access_token)
  end
end