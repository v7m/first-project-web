class ChatRoom < ApplicationRecord
  has_many :messages, dependent: :destroy

  has_many :user_chat_rooms
  has_many :users, through: :user_chat_rooms

  def self.create_for_users(first_user, second_user)
    chat_room = ChatRoom.create
    [first_user, second_user].each{ |user| UserChatRoom.create(user: user, chat_room: chat_room) }
    chat_room
  end

  def self.find_for_users(first_user, second_user)
    (first_user.chat_rooms & second_user.chat_rooms).first
  end

  def self.find_or_create_for_users(first_user, second_user)
    self.find_for_users(first_user, second_user) || self.create_for_users(first_user, second_user)
  end

  def chanel
    "chat_room_#{id}"
  end

  def interlocutor_for(user)
    users.where.not(id: user.id).first
  end
end
