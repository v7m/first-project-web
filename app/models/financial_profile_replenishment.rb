class FinancialProfileReplenishment < ApplicationRecord
  belongs_to :task
  belongs_to :financial_profile

  validates :amount, presence: true

  after_create :update_financial_profile_amount

  private

  def update_financial_profile_amount
    financial_profile.deposit!(amount)
  end
end