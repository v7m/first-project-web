class Donate < ApplicationRecord
  belongs_to :user
  belongs_to :task

  validates :transaction_id, :amount, presence: true

  enum status: { created: 0, refunded: 1 }

  before_create :check_task_state
  after_create :collect_whole_task_amount, if: :last?

  def last?
    task.donated_whole_amount?
  end

  private

  def check_task_state
    unless task.approved?
      errors.add(:task, 'is not available for donating')
    end
  end

  def collect_whole_task_amount
    task.collect_amount!
  end
end
