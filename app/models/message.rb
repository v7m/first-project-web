class Message < ApplicationRecord
  belongs_to :chat_room
  belongs_to :user

  validates :body, presence: true

  after_create :send_to_pusher, :send_push_notification

  private

  def send_to_pusher
    Pusher::SendMessage.new(self).perform
  end

  def send_push_notification
  	PushNotifications::MessageCreated.new(self).perform
  end
end
