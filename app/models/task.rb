class Task < ApplicationRecord
  include AASM

  belongs_to :creator, class_name: 'User', foreign_key: 'user_id'
  belongs_to :doer, class_name: 'User', foreign_key: 'doer_id', optional: true

  has_many :comments, dependent: :destroy
  has_many :commentators, through: :comments,
           foreign_key: 'user_id',
           class_name: 'User',
           source: :user

  has_many :likes
  has_many :liked_users, through: :likes,
           foreign_key: 'user_id',
           class_name: 'User',
           source: :user

  has_many :donates
  has_many :donaters, through: :donates,
           foreign_key: 'user_id',
           class_name: 'User',
           source: :user


  has_one :financial_profile_replenishment

  enum state: {
      not_approved: 0,
      refused: 1,
      approved: 2,
      amount_collected: 3,
      accomplished: 4,
      expired: 5,
      rejected: 6,
      closed: 7
  }

  enum category: {
      undefined: 0,
      angel: 1,
      demon: 2
  }

  validates :title, :description, :launch_at, :finish_at, :desired_rate, presence: true
  validates :video_url, :image_url, :accomplish_video_url, :accomplish_image_url, url: { allow_nil: true, allow_blank: true }
  validate :check_status, on: :update, if: :doer_id_changed?

  after_update :send_doer_assigned_push_notification, if: :doer_id_changed?

  default_scope { order("created_at DESC") }

  scope :filter, ->(filter_params) {
    if filter_params.present?
      filter_by_title(filter_params[:title]).filter_by_desired_rate(filter_params[:desired_rate]).filter_by_created_at(filter_params[:created_at])
    end
  }

  scope :filter_by_title, ->(title) {
    title = title.downcase if title.present?
    where("LOWER(title) LIKE lower(:title)", title: "%#{title}%")
  }

  scope :filter_by_desired_rate, ->(desired_rate) {
    where('desired_rate <= ?', desired_rate.to_f) if desired_rate.present? 
  }

  scope :filter_by_created_at, ->(created_at) {
    if created_at.present?
      created_at = DateTime.parse(created_at)
      where('created_at >= ?', created_at)
    end  
  }

  aasm column: :state, enum: true do
    state :not_approved, initial: true
    state :refused
    state :approved
    state :amount_collected
    state :expired
    state :accomplished
    state :rejected
    state :closed

    event :refuse do
      transitions from: :not_approved, to: :refused
    end

    event :approve do
      after { initialize_expiration! }
      transitions from: :not_approved, to: :approved
    end

    event :collect_amount do
      after { send_amount_collected_push_notification }
      transitions from: :approved, to: :amount_collected
    end

    event :expire do
      after { refund_donates! }
      transitions from: [:approved, :amount_collected], to: :expired
    end

    event :accomplish do
      after { cancel_expiration! }
      transitions from: [:approved, :amount_collected], to: :accomplished
    end

    event :reject do
      after { refund_donates! }
      transitions from: :accomplished, to: :rejected
    end

    event :close do
      after do
        create_financial_profile_replenishment!
        send_closed_push_notification
      end
      transitions from: :accomplished, to: :closed
    end
  end

  def created_by?(user)
    self.creator == user
  end

  def doing_by?(user)
    self.doer == user
  end

  def assign_doer(user)
    self.doer = user
    save
  end

  def like(user)
    return true if liked_by?(user)
    user.liked_tasks << self
  end

  def unlike(user)
    user.liked_tasks.delete(self)
  end

  def liked_by?(user)
    liked_users.include?(user)
  end

  def donated_amount
    Donate.where(task: self).pluck(:amount).inject(0){ |sum, x| sum + x.to_f }.to_s
  end

  def donated_whole_amount?
    donated_amount.to_f >= desired_rate.to_f
  end

  def can_expire?
    approved? || amount_collected?
  end

  def has_financial_profile_replenishment?
    financial_profile_replenishment.present?
  end

  def add_to_user_gallery(user)
    return true if in_user_gallery?(user)
    user.tasks_in_gallery << self
  end

  def remove_from_user_gallery(user)
    user.tasks_in_gallery.delete(self)
  end

  def in_user_gallery?(user)
    user.tasks_in_gallery.include?(self)
  end

  private

  def check_status
    unless approved? || amount_collected?
      errors.add(:status, 'is not approved')
    end
  end

  def refund_donates!
    donates.each{ |donate| DonateRefundWorker.perform_async(donate.id) }
  end

  def initialize_expiration!
    expiration_job_id = TaskExpirationWorker.perform_at(finish_at, id)
    update(expiration_job_id: expiration_job_id)
  end

  def cancel_expiration!
    Sidekiq::ScheduledSet.new.select{ |job_id| expiration_job_id }.each(&:delete)
    update(expiration_job_id: nil)
  end

  def create_financial_profile_replenishment!
    FinancialProfileReplenishment.create(task: self, financial_profile: doer.financial_profile, amount: donated_amount)
  end

  def send_doer_assigned_push_notification
    TaskDoerAssignedWorker.perform_async(id)
  end

  def send_amount_collected_push_notification
    TaskAmountCollectedWorker.perform_async(id)
  end

  def send_closed_push_notification
    TaskClosedWorker.perform_async(id)
  end
end
