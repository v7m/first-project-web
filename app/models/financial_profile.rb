class FinancialProfile < ApplicationRecord
  belongs_to :user

  has_many :deposits
  has_many :money_withdrawals

  has_many :financial_profile_replenishments
  has_many :tasks, through: :financial_profile_replenishments,
           foreign_key: 'task_id',
           class_name: 'Task',
           source: :task

  validates :amount, presence: true
  validates :amount, numericality: { greater_than_or_equal_to: 0 }

  def debit!(amount_for_debit)
    update_amount!(amount - amount_for_debit.to_f)
  end

  def deposit!(amount_for_deposit)
    update_amount!(amount + amount_for_deposit.to_f)
  end

  def enough_for_debit?(amount_for_debit)
    amount >= amount_for_debit.to_f
  end

  private

  def update_amount!(amount)
    self.amount = amount.to_f
    save!
  end
end