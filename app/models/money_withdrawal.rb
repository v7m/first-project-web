class MoneyWithdrawal < ApplicationRecord
  belongs_to :financial_profile

  validates :amount, presence: true

  before_validation :check_financial_profile_amount
  after_create :update_financial_profile_amount

  private

  def update_financial_profile_amount
    financial_profile.debit!(amount)
  end

  def check_financial_profile_amount
    if financial_profile.amount < amount
      errors.add(:amount, 'not enough money')
    end
  end
end