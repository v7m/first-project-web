class User < ApplicationRecord
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  mount_uploader :avatar, AvatarUploader

  has_one :financial_profile, dependent: :destroy
  has_many :sessions, dependent: :destroy
  has_many :messages, dependent: :destroy

  has_many :created_tasks, dependent: :destroy, class_name: 'Task', foreign_key: 'user_id'
  has_many :doing_tasks, dependent: :destroy, class_name: 'Task', foreign_key: 'doer_id'

  has_many :comments, dependent: :destroy
  has_many :commented_tasks, through: :comments,
           foreign_key: 'task_id',
           class_name: 'Task',
           source: :task

  has_many :likes
  has_many :liked_tasks, through: :likes,
           foreign_key: 'task_id',
           class_name: 'Task',
           source: :task

  has_many :donates
  has_many :donated_tasks, through: :donates,
           foreign_key: 'task_id',
           class_name: 'Task',
           source: :task

  has_many :user_chat_rooms
  has_many :chat_rooms, through: :user_chat_rooms

  has_many :gallery_tasks
  has_many :tasks_in_gallery, through: :gallery_tasks,
           foreign_key: 'task_id',
           class_name: 'Task',
           source: :task

  has_many :blocked_user_relationships, foreign_key: :blocked_user_id, class_name: 'BlockedUsers'
  has_many :users_who_blocked, through: :blocked_user_relationships, source: :user

  has_many :blocked_user_relationships, foreign_key: :user_id, class_name: 'BlockedUsers'
  has_many :blocked_users, through: :blocked_user_relationships, source: :blocked_user

  has_many :follower_relationships, foreign_key: :following_id, class_name: 'Follow'
  has_many :followers, through: :follower_relationships, source: :follower

  has_many :following_relationships, foreign_key: :follower_id, class_name: 'Follow'
  has_many :following, through: :following_relationships, source: :following

  validates :username, presence: true, uniqueness: true
  validates :email, presence: true, email: true

  after_create :create_new_financial_profile

  scope :filter_by_username, ->(username) {
    username = username.downcase if username.present?
    where("LOWER(username) LIKE lower(:username)", username: "%#{username}%")
  }

  def chat_room_event
    "new_message_for_#{id}"
  end

  def follow(user)
    following_relationships.create(following: user)
  end

  def unfollow(user)
    following_relationships.find_by(following: user).destroy
  end

  def follow?(user)
    following_relationships.find_by(following: user).present?
  end

  def not_follow?(user)
    !follow?(user)
  end

  def has_double_side_following?(user)
    follow?(user) && user.follow?(self)
  end

  def block(user)
    blocked_user_relationships.create(blocked_user: user)
  end

  def unblock(user)
    blocked_user_relationships.find_by(blocked_user: user).destroy
  end

  def blocked?(user)
    blocked_user_relationships.find_by(blocked_user: user).present?
  end

  def not_blocked?(user)
    !blocked?(user)
  end

  def can_message_to_chat_room?(chat_room)
    chat_room.interlocutor_for(self).not_blocked?(self)
  end

  def set_customer_id(customer_id)
    update(braintree_customer_id: customer_id)
  end

  def has_payment_info?
    braintree_customer_id
  end

  def has_chat_room?(chat_room)
    user_chat_rooms.where(chat_room: chat_room).any?
  end

  private

  def create_new_financial_profile
    create_financial_profile
  end
end
