class PushNotifications::TaskAmountCollected < PushNotifications::Base
  CATEGORY = 'TASK_AMOUNT_COLLECTED'

  def initialize(task)
    @task = task
    @alert = "Your task #{@task.title} amount has been collected"
  end

  def perform
    send_push(@task.doer, @alert, CATEGORY)
  end
end