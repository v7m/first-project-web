class PushNotifications::MessageCreated < PushNotifications::Base
  CATEGORY = 'MESSAGE_CREATED'

  def initialize(message)
    @message = message
    @alert = @message.body

    chat_room = ChatRoom.find(@message.chat_room_id)
    user = User.find(@message.user_id)
    @interlocutor = chat_room.interlocutor_for(user)
  end

  def perform
    send_push(@interlocutor, @alert, CATEGORY)
  end
end