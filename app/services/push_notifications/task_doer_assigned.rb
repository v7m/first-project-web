class PushNotifications::TaskDoerAssigned < PushNotifications::Base
  CATEGORY = 'TASK_DOER_ASSIGNED'

  def initialize(task)
    @task = task
    @alert = "Your task #{@task.title} assigned to #{@task.doer.username}"
  end

  def perform
    send_push(@task.creator, @alert, CATEGORY)
  end
end