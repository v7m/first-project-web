class PushNotifications::Base
  def send_push(recipient, alert, category)
    SendPushNotifications::Apn.new(recipient, alert, category).perform if recipient.present?
  end
end