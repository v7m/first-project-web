class PushNotifications::TaskClosed < PushNotifications::Base
  CATEGORY = 'TASK_CLOSED'

  def initialize(task)
    @task = task
    @alert = "Your task #{@task.title} has been closed"
  end

  def perform
    send_push(@task.doer, @alert, CATEGORY)
  end
end