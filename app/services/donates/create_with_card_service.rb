module Donates

  class CreateWithCardService

    def initialize(user, task, amount, payment_method_nonce)
      @user, @task, @amount, @payment_method_nonce = user, task, amount, payment_method_nonce
    end

    def perform
      result = Braintree::CreateTransactionService.new(
          @amount,
          @payment_method_nonce
      ).perform

      if result.success?
        Donate.create(
            task: @task,
            user: @user,
            transaction_id: result.transaction.id,
            amount: result.transaction.amount.to_f
        )
      end
      result
    end

  end

end