module Donates

  class CreateWithAmountService

    def initialize(user, task, amount)
      @user = user
      @task = task
      @amount = amount
    end

    def perform
      Donate.transaction do
        @user.financial_profile.debit!(@amount)
        Donate.create(
            task: @task,
            user: @user,
            transaction_id: 'from_amount',
            amount: @amount
        )
      end
    end

  end

end