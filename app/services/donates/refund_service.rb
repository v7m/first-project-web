module Donates

  class RefundService

    def initialize(donate)
      @donate = donate
    end

    def perform
      result = Braintree::CreateRefundService.new(@donate.transaction_id).perform
      donate.refund! if result.success?
      result
    end
  end

end