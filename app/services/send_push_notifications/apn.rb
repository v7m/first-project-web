class SendPushNotifications::Apn < SendPushNotifications::Base
  SOUND = 'sosumi.aiff'

  def initialize(recipient, alert, category)
    @recipient, @alert, @category = recipient, alert, category
  end

  def perform
    send_notifications(@recipient.sessions)
  end

  private

  def send_notifications(sessions)
    sessions.pushable.each{ |session| send_notification(session.push_token) }
  end

  def send_notification(push_token)
    APN.push(notification(push_token))
  end

  def notification(push_token)
    Houston::Notification.new(data(push_token))
  end

  def data(push_token)
    {
      device: push_token,
      alert: @alert,
      sound: SOUND,
      category: @category,
      content_available: true
    }
  end
end