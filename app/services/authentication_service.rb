class AuthenticationService
  attr_reader :user, :session

  def initialize(user, session_params)
    @user = user
    @session_params = session_params
    @session = nil
  end

  def authenticate!
    @session = Session.create_for(@user, @session_params)
  end

  def errors
    @session.errors
  end
end