module Deposits

  class CreateService

    def initialize(user, amount, payment_method_nonce)
      @user = user
      @amount = amount
      @payment_method_nonce = payment_method_nonce
    end

    def perform
      result = Braintree::CreateTransactionService.new(
          @amount,
          @payment_method_nonce
      ).perform

      if result.success?
        Deposit.create(
            financial_profile: @user.financial_profile,
            transaction_id: result.transaction.id,
            amount: result.transaction.amount.to_f
        )
      end
      result
    end

  end
end