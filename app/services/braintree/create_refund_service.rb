module Braintree

  class Braintree::CreateRefundService

    def initialize(transaction_id)
      @transaction_id = transaction_id
    end

    def perform
      Braintree::Transaction.refund(@transaction_id)
    end

  end

end