module Braintree

  class Braintree::CreateTransactionService

    def initialize(amount, payment_method_nonce)
      @amount, @payment_method_nonce = amount, payment_method_nonce
    end

    def perform
      Braintree::Transaction.sale(transaction_params)
    end

    private

    def transaction_params
      {
          amount: @amount.to_s,
          payment_method_nonce: @payment_method_nonce
      }
    end

  end

end