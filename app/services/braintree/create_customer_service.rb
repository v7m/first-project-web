module Braintree

  class CreateCustomerService

    def initialize(user, payment_method_nonce)
      @user, @payment_method_nonce = user, payment_method_nonce
    end

    def perform
      result = Braintree::Customer.create(
          email: @user.email,
          payment_method_nonce: @payment_method_nonce
      )
      @user.set_customer_id(result.customer.id) if result.success?
      result
    end

  end

end