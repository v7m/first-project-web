class Pusher::SendMessage

  def initialize(message)
    @message = message
    @chat_room = ChatRoom.find(message.chat_room_id)
    @user = User.find(message.user_id)
    @event = @chat_room.interlocutor_for(@user).chat_room_event
  end

  def perform
    Pusher.trigger(@chat_room.chanel, @event, data)
  end

  private

  def data
    {
        chat_room_id: @message.chat_room_id,
        body: @message.body,
        created_at: @message.created_at,
        user: {
          id: @user.id,
          email: @user.email,
          username: @user.username,
          avatar: @user.avatar.url,
          status: @user.status
        }
    }
  end

end