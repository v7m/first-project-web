class TaskExpirationWorker
  include Sidekiq::Worker

  def perform(task_id)
    task = Task.find(task_id)
    task.expire! if task.can_expire?
  end
end
