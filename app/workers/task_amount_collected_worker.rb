class TaskAmountCollectedWorker
  include Sidekiq::Worker

  def perform(task_id)
    task = Task.find(task_id)
    PushNotifications::TaskAmountCollected.new(task).perform
  end
end
