class TaskClosedWorker
  include Sidekiq::Worker

  def perform(task_id)
    task = Task.find(task_id)
    PushNotifications::TaskClosed.new(task).perform
  end
end
