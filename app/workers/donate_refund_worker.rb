class DonateRefundWorker
  include Sidekiq::Worker

  def perform(donate_id)
    Donates::RefundService.new(Donate.find(donate_id)).perform
  end
end
