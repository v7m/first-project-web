class TaskDoerAssignedWorker
  include Sidekiq::Worker

  def perform(task_id)
    task = Task.find(task_id)
    PushNotifications::TaskDoerAssigned.new(task).perform
  end
end
