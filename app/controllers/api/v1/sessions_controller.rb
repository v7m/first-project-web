class Api::V1::SessionsController < Api::V1::ApiController
  def destroy_session
    @current_session.destroy

    render_successful_success
  end

  def set_push_token
    if @current_session.update_attributes(session_params)
      render_successful_success
    else
      render_unprocessable_entity_for(@current_session)
    end
  end

  private

  def session_params
    params.require(:session).permit(:push_token)
  end
end