class Api::V1::Tasks::CommentsController < Api::V1::ApiController
  before_action :get_task

  # GET /tasks/:task_id/comments
  def index
    @comments = @task.comments
  end

  # POST /tasks/:task_id/comments
  def create
    @comment = Comment.new(comment_params)
    @comment.task = @task
    @comment.user = @current_user

    unless @comment.save
      render_unprocessable_entity_for(@comment)
    end
  end

  private

  def get_task
    @task = Task.find(params[:task_id])
  end

  def comment_params
    params.require(:comment).permit(:text)
  end
end