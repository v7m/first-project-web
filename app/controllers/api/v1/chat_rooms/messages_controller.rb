class Api::V1::ChatRooms::MessagesController < Api::V1::ApiController
  before_action :get_chat_room
  before_action :check_chat_room!

  # GET /api/v1/chat_rooms/:chat_room_id/messages
  def index
    @messages = @chat_room.messages
  end

  # POST /api/v1/chat_rooms/:chat_room_id/messages
  def create
    if @current_user.can_message_to_chat_room?(@chat_room)
      @message = Message.new(message_params)
      @message.user = @current_user
      @message.chat_room = @chat_room

      unless @message.save
        render_unprocessable_entity_for(@message)
      end
    else
      render_unauthorized(I18n.t('controllers.api.v1.messages.create.blocked'))
    end
  end

  private

  def get_chat_room
    @chat_room = ChatRoom.find(params[:chat_room_id])
  end

  def check_chat_room!
    unless @current_user.has_chat_room?(@chat_room)
      render_bad_request(I18n.t('controllers.api.v1.messages.create.no_chat_room'))
    end
  end

  def message_params
    params.require(:message).permit(:body)
  end
end
