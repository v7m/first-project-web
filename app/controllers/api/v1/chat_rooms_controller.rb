class Api::V1::ChatRoomsController < Api::V1::ApiController
  before_action :get_interlocutor, only: [:get_chat_room]

  def index
    @chat_rooms = @current_user.chat_rooms
  end

  def get_chat_room
    @chat_room = ChatRoom.find_or_create_for_users(@current_user, @interlocutor)
  end

  private

  def get_interlocutor
    @interlocutor = User.find(params[:interlocutor_id])
  end
end