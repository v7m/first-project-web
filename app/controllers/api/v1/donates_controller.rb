class Api::V1::DonatesController < Api::V1::ApiController
  before_action :get_task, only: [:create_with_card, :create_with_amount]

  def new
    @client_token = generate_braintree_client_token
  end

  def create_with_card
    result = Donates::CreateWithCardService.new(
        @current_user,
        @task,
        params[:donate][:amount],
        params[:payment_method_nonce]
    ).perform

    if result.success?
      render_successful_success
    else
      render_bad_request(I18n.t('controllers.api.v1.donates.create.fail'))
    end
  end

  def create_with_amount
    if @current_user.financial_profile.enough_for_debit?(params[:donate][:amount])
      result = Donates::CreateWithAmountService.new(
          @current_user,
          @task,
          params[:donate][:amount]
      ).perform

      if result
        render_successful_success
      else
        render_bad_request(I18n.t('controllers.api.v1.donates.create.fail'))
      end
    else
      render_bad_request(I18n.t('controllers.api.v1.donates.create.not_enough_money'))
    end
  end

  private

  def get_task
    @task = Task.find(params[:donate][:task_id])
  end
end