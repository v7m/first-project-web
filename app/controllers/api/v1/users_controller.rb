class Api::V1::UsersController < Api::V1::ApiController
  skip_before_action :current_user, only: :reset_password
  before_action :get_user, only: [:profile, :update_profile]
  before_action :get_following_user, only: [:follow, :unfollow]


  # GET /users/:id
  def index
    @users = User.all.filter_by_username(params[:filter_username])
  end

  # GET /users/:id
  def show
    @user = User.find(params[:id])
  end

  # GET /users/profile
  def profile
    render 'api/v1/users/current_user'
  end

  # GET /users/followers
  def followers
    @users = @current_user.followers.filter_by_username(params[:filter_username])
  end

  # GET /users/following
  def following
    @users = @current_user.following.filter_by_username(params[:filter_username])
  end

  # PUT /users/update_profile
  def update_profile
    if @user.update(user_params)
      render 'api/v1/users/current_user'
    else
      render_unprocessable_entity_for(@user)
    end
  end

  # PUT /users/update_password
  def update_password
    if @current_user.valid_password?(params[:user][:current_password])
      if password_confirmation_valid?
        if @current_user.update(user_password_params)
          render_successful_success
        else
          render_unprocessable_entity_for(@user)
        end
      else
        render_unprocessable_entity(I18n.t('controllers.api.v1.users.update_password.password_confirmation_invalid'))
      end
    else
      render_unprocessable_entity(I18n.t('controllers.api.v1.users.update_password.old_password_invalid'))
    end
  end

  # GET /users/reset_password
  def reset_password
    @user = User.find_by_email!(user_params[:email])
    if @user.present?
      @user.send_reset_password_instructions
      render_success(I18n.t('controllers.api.v1.users.reset_password.success'))
    end
  end

  # POST /users/:id/follow
  def follow
    if @current_user.not_follow?(@following_user)
      @current_user.follow(@following_user)

      render_successful_success
    else
      render_bad_request(I18n.t('controllers.api.v1.users.follow.already_follow'))
    end
  end

  # DELETE /users/:id/unfollow
  def unfollow
    if @current_user.follow?(@following_user)
      @current_user.unfollow(@following_user)

      render_successful_success
    else
      render_bad_request(I18n.t('controllers.api.v1.users.unfollow.not_follow_yet'))
    end
  end

  # POST /users/withdraw_money
  def withdraw_money
    @money_withdrawal = MoneyWithdrawal.new(amount: params[:amount], financial_profile: @current_user.financial_profile)

    if @money_withdrawal.save
      render_successful_success
    else
      render_unprocessable_entity_for(@money_withdrawal)
    end
  end

  private

  def get_user
    @user = @current_user
  end

  def get_following_user
    @following_user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:email, :username, :avatar, :status, :position)
  end

  def user_password_params
    params.require(:user).permit(:password, :password_confirmation)
  end

  def password_confirmation_valid?
    params[:user][:password] == params[:user][:password_confirmation]
  end
end