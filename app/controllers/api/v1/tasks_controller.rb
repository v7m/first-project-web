class Api::V1::TasksController < Api::V1::ApiController
  before_action :get_task, only: [:show, :accomplish, :assign_to_me, :like, :unlike, :add_to_gallery, :remove_from_gallery]

  # GET /tasks/approved
  def approved
    @tasks = Task.where(state: ['approved', 'amount_collected']).filter(params[:filter]).page params[:page]
  end

  # GET /tasks/closed
  def closed
    @tasks = Task.closed.filter(params[:filter]).page params[:page]
  end

  # GET /tasks/:id
  def show
  end

  # POST /tasks
  def create
    @task = Task.new(task_params)
    @task.creator = @current_user

    unless @task.save
      render_unprocessable_entity_for(@task)
    end
  end

  # GET /tasks/created
  def created
    @tasks = @current_user.created_tasks.filter(params[:filter]).page params[:page]
  end

  # GET /tasks/doing
  def doing
    @tasks = @current_user.doing_tasks.filter(params[:filter]).page params[:page]
  end

  # GET /tasks/in_gallery
  def in_gallery
    @tasks = @current_user.tasks_in_gallery.filter(params[:filter]).page params[:page]
  end

  # PUT /tasks/:id/assign_to_me
  def assign_to_me
    if @task.assign_doer(@current_user)
      render_success(I18n.t('controllers.api.v1.tasks.assign_to_me.success'))
    else
      render_unprocessable_entity_for(@task)
    end
  end

  # PUT /tasks/:id/accomplish
  def accomplish
    if @task.doing_by?(@current_user)
      if @task.update(task_accomplish_params)
        @task.accomplish
      else
        render_unprocessable_entity_for(@task)
      end
    else
      render_unauthorized(I18n.t('controllers.api.v1.tasks.update_video.unauthorized'))
    end
  end

  # POST /tasks/:id/like
  def like
    @task.like(@current_user)
    render_successful_success
  end

  # DELETE /tasks/:id/unlike
  def unlike
    @task.unlike(@current_user)
    render_successful_success
  end

  # POST /tasks/:id/add_to_gallery
  def add_to_gallery
    @task.add_to_user_gallery(@current_user)
    render_successful_success
  end

  # DELETE /tasks/:id/remove_from_gallery
  def remove_from_gallery
    @task.remove_from_user_gallery(@current_user)
    render_successful_success
  end

  private

  def get_task
    @task = Task.find(params[:id])
  end

  def task_params
    params.require(:task).permit(
      :title,
      :description,
      :category,
      :video_url,
      :image_url,
      :desired_rate,
      :launch_at,
      :finish_at,
      :doc_proof,
      :manage,
      :latitude,
      :longitude,
      items: [])
  end

  def task_accomplish_params
    params.require(:task).permit(:accomplish_video_url, :accomplish_image_url)
  end
end