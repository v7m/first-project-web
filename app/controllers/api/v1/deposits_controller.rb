class Api::V1::DepositsController < Api::V1::ApiController

  def new
    @client_token = generate_braintree_client_token
  end

  def create
    result = Deposits::CreateService.new(
      @current_user,
      params[:deposits][:amount],
      params[:payment_method_nonce]
    ).perform

    if result.success?
      render_successful_success
    else
      render_bad_request(I18n.t('controllers.api.v1.donates.create.fail'))
    end
  end

end