class Api::V1::AuthenticationsController < Api::V1::ApiController
  skip_before_action :current_user

  # POST /auth/sign_up
  def sign_up
    if password_confirmation_valid?
      @user = User.new(user_params)
      if @user.save
        sign_in_user
      else
        render_unprocessable_entity(errors_for(@user))
      end
    else
      render_unprocessable_entity(I18n.t('controllers.api.v1.authentications.register.password_convirmation_invalid'))
    end
  end

  # POST /auth/login
  def login
    @user = User.find_by_email(user_params[:email])
    if @user.present? && @user.valid_password?(user_params[:password])
      sign_in_user
    else
      render_bad_request(I18n.t('controllers.api.v1.authentications.login.invalid_email_or_password'))
    end
  end

  protected

  def sign_in_user
    create_session_for_user
  end

  def create_session_for_user

    auth = AuthenticationService.new(@user, session_params)
    auth.authenticate!
    if auth.errors.empty?
      set_access_token(auth.session)

      render 'api/v1/users/current_user'
    else
      render_unprocessable_entity(errors_for(auth.session))
    end
  end

  def session_params
    params.require(:session).permit(:device_token, :device_platform)
  end

  def user_params
    params.require(:user).permit(:email, :password, :username)
  end

  def password_confirmation_valid?
    params[:user][:password] == params[:user][:password_confirmation]
  end
end