class Api::V1::ApiController < ApplicationController
  # protect_from_forgery with: :null_session

  before_action :current_user

  respond_to :json

  SessionError = Class.new(StandardError)

  rescue_from ActiveRecord::RecordNotFound, with: :render_not_found
  rescue_from ActionController::ParameterMissing, with: :render_parameter_missing
  rescue_from SessionError, with: :render_unauthorized
  rescue_from StandardError, with: :render_bad_request

  def current_user
    @current_user ||= current_session.try(:user)
  end

  def current_session
    @current_session ||= get_session
  end

  protected

  def generate_braintree_client_token
    if @current_user.has_payment_info?
      Braintree::ClientToken.generate(customer_id: @current_user.braintree_customer_id)
    else
      Braintree::ClientToken.generate
    end
  end

  def get_session
    session = Session.find_by(access_token: request.headers['Access-Token'])
    render_session_expired and return unless session
    set_access_token(session) if session.touch
    session
  end

  def set_access_token(session)
    response.headers['Access-Token'] = session.access_token
  end

  def render_error(status, type, message)
    json = { type: type, message: message }
    render json: json, status: status
  end

  def render_success(message)
    json = { message: message }
    render json: json, status: 200
  end

  def render_successful_success
    render_success('Success!')
  end

  def render_not_found(e)
    render_error(404, 'NotFound', e.message.sub(/ with.*/, ''))
  end

  def render_bad_request(message)
    render_error(400, 'BadRequest', message)
  end

  def render_unauthorized(message)
    render_error(401, 'Unauthorized', message)
  end

  def render_session_expired
    render_unauthorized(I18n.t('controllers.api.v1.api.render_session_expired.expired'))
  end

  def render_parameter_missing(e)
    render_error(422, 'ParameterMissing', e.message)
  end

  def render_unprocessable_entity(message)
    render_error(422, 'UnprocessableEntity', message)
  end

  def render_unprocessable_entity_for(entry)
    render_error(422, 'UnprocessableEntity', errors_for(entry))
  end

  def errors_for(entry)
    entry.errors.full_messages.join('; ')
  end
end