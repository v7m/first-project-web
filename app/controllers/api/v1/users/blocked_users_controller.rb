class Api::V1::Users::BlockedUsersController < Api::V1::ApiController
  before_action :get_blocked_user

  def create
    if @current_user.not_blocked?(@blocked_user)
      @current_user.block(@blocked_user)

      render_successful_success
    else
      render_bad_request(I18n.t('controllers.api.v1.users.blocked_users.create.already_blocked'))
    end
  end

  private

  def get_blocked_user
    @blocked_user = User.find(params[:blocked_user_id])
  end
end