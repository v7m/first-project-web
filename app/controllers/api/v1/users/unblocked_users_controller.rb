class Api::V1::Users::UnblockedUsersController < Api::V1::ApiController
  before_action :get_unblocked_user

  def destroy
    if @current_user.blocked?(@unblocked_user)
      @current_user.unblock(@unblocked_user)

      render_successful_success
    else
      render_bad_request(I18n.t('controllers.api.v1.users.unblocked_users.delete.not_blocked_yet'))
    end
  end

  private

  def get_unblocked_user
    @unblocked_user = User.find(params[:id])
  end
end