class Api::V1::Users::TasksController < Api::V1::ApiController
  before_action :get_user

  # GET /users/:user_id/tasks/created
  def created
    @tasks = @user.created_tasks.filter(params[:filter])
  end

  # GET /users/:user_id/tasks/doing
  def doing
    @tasks = @user.doing_tasks.filter(params[:filter])
  end

  private

  def get_user
    @user = User.find(params[:user_id])
  end

end