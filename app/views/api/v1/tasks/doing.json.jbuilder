json.data do
  json.partial! 'api/v1/tasks/task', collection: @tasks, as: :task
end

json.total_pages	@tasks.total_pages
json.current_page	@tasks.current_page
json.next_page		@tasks.next_page
json.prev_page		@tasks.prev_page