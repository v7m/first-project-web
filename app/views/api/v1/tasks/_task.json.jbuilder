json.(task, :id, :title, :description, :state, :category, :items, :user_id, :doer_id, :video_url, :image_url, :accomplish_video_url, :accomplish_image_url, :launch_at, :finish_at, :doc_proof, :manage)

json.location do
	json.latitude			        task.latitude.to_f
	json.longitude			      task.longitude.to_f
end

json.desired_rate   		    task.desired_rate.to_f
json.donated_amount         task.donated_amount.to_f
json.likes          		    task.likes.count
json.liked_by_current_user	task.liked_by?(@current_user)	

json.creator_username 		  task.creator.username
json.creator_avatar    		  task.creator.avatar.url

json.doer_username 			    task.doer.present? ? task.doer.username : nil
json.doer_avatar    		    task.doer.present? ? task.doer.avatar.url : nil