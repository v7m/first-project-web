json.partial! 'api/v1/tasks/task', task: @task

json.comments do
  json.partial! 'api/v1/comments/comment', collection: @task.comments, as: :comment
end