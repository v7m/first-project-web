json.(message, :id, :chat_room_id, :body, :created_at)

json.user do
  json.partial! 'api/v1/users/user', user: message.user
end