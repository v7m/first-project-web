json.id       chat_room.id
json.chanel   chat_room.chanel

json.users do
  json.partial! 'api/v1/users/user', collection: chat_room.users, as: :user
end

if chat_room.messages.last.present?
  json.last_message do
    json.partial! 'api/v1/messages/message', message: chat_room.messages.last
  end
else
  json.last_message   nil
end