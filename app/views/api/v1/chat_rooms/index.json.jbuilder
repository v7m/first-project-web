json.chat_rooms do
  json.partial! 'api/v1/chat_rooms/chat_room', collection: @chat_rooms, as: :chat_room
end