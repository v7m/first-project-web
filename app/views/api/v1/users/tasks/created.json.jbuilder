json.data do
  json.partial! 'api/v1/tasks/task', collection: @tasks, as: :task
end