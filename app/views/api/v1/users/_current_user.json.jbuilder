json.id                 user.id
json.email              user.email
json.username           user.username
json.avatar             user.avatar.url
json.status             user.status
json.position           user.position
json.amount             user.financial_profile.amount.to_f
json.angel_tasks_coefficient    user.doing_tasks.angel.count / user.doing_tasks.count.to_f
json.demon_tasks_coefficient    user.doing_tasks.demon.count / user.doing_tasks.count.to_f