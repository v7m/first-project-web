json.id         	  comment.id
json.user_id    	  comment.user_id
json.task_id    	  comment.task_id
json.text       	  comment.text
json.created_at     comment.created_at
json.user_username 	comment.user.username
json.avatar         comment.user.avatar.url