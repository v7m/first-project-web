RailsAdmin.config do |config|

  config.main_app_name = ["First SEX"]
  config.parent_controller = '::RailsAdminCustomController'
  config.included_models = [User, Task, Comment]
  config.total_columns_width = 1100

  ### Popular gems integration

  ## == Devise ==
  # config.authenticate_with do
  #   warden.authenticate! scope: :user
  # end
  # config.current_user_method(&:current_user)

  ## == Cancan ==
  # config.authorize_with :cancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  ## == Gravatar integration ==
  ## To disable Gravatar integration in Navigation Bar set to false
  # config.show_gravatar true

  config.actions do
    state
  end

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new do
      only []
    end
    export do
      only []
    end
    bulk_delete do
      only []
    end
    show
    edit do
      only ['User', 'Task', 'Comment']
    end
    delete do
      only ['Task', 'Comment']
    end
    show_in_app

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end

  config.model 'User' do
    list do
      field :id
      field :email
      field :username
      field :avatar
    end

    show do
      field :id
      field :email
      field :username
      field :avatar
      field :created_at
      field :sign_in_count
      field :tasks
      field :comments
    end

    update do
      field :email
      field :username
      field :avatar
    end
  end

  config.model 'Task' do
    list do
      field :id
      field :title
      field :state, :state do
        formatted_value do
          value.humanize
        end
      end
      field :creator
      field :doer
      field :description
      field :desired_rate
      field :launch_at
      field :finish_at
      field :created_at
    end

    show do
      field :id
      field :title
      field :state, :state do
        formatted_value do
          value.humanize
        end
      end
      field :creator
      field :doer
      field :description
      field :items
      field :video_url do
        pretty_value do
          url = bindings[:object].video_url
          url.present? ? %{<video src="#{url}" controls="controls"></video>}.html_safe : nil
        end
      end
      field :image_url do
        pretty_value do
          url = bindings[:object].image_url
          url.present? ? %{<img src="#{url}" style="max-width: 640px;"></img>}.html_safe : nil
        end
      end
      field :accomplish_video_url do
        pretty_value do
          url = bindings[:object].accomplish_video_url
          url.present? ? %{<video src="#{url}" controls="controls"></video>}.html_safe : nil
        end
      end
      field :accomplish_image_url do
        pretty_value do
          url = bindings[:object].accomplish_image_url
          url.present? ? %{<img src="#{url}" style="max-width: 640px;"></img>}.html_safe : nil
        end
      end
      field :desired_rate
      field :launch_at
      field :finish_at
      field :doc_proof
      field :manage
      field :created_at
    end

    update do
      field :title
      field :description
      field :state, :state do
        formatted_value do
          value.humanize
        end
      end
      field :video_url
      field :image_url
      field :accomplish_video_url
      field :accomplish_image_url
      field :desired_rate
      field :launch_at
      field :finish_at
      field :doc_proof
      field :manage
    end

    state({
      events: {
          refuse: 'btn-danger',
          approve: 'btn-primary',
          collect_amount: 'btn-primary',
          expire: 'btn-danger',
          accomplish: 'btn-warning',
          reject: 'btn-danger',
          close: 'btn-success'
      },
      states: {
          refused: 'label-danger',
          approved: 'label-primary',
          amount_collected: 'label-primary',
          expired: 'label-danger',
          accomplished: 'label-warning',
          rejected: 'label-danger',
          closed: 'label-success'
      },
      disable: [:collect_amount, :expire, :accomplish]
    })
  end

  config.model 'Comment' do
    list do
      field :id
      field :user
      field :text
      field :task
    end

    show do
      field :id
      field :text
      field :user
      field :task
      field :created_at
    end

    update do
      field :text
    end
  end
end
