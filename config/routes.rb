Rails.application.routes.draw do
  require 'sidekiq/web'

  mount Sidekiq::Web => "/sidekiq"
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  devise_for :users

  namespace :api, defaults: { format: :json } do
    namespace :v1 do

      resources :auth, controller: 'authentications', only: [] do
        post :sign_up, on: :collection
        post :login, on: :collection
      end

      resources :sessions, only: [] do
        put :set_push_token, on: :collection
        delete :destroy_session, on: :collection
      end

      resources :users, only: [:index, :show] do
        get :reset_password, on: :collection
        get :profile, on: :collection
        get :followers, on: :collection
        get :following, on: :collection
        put :update_profile, on: :collection
        put :update_password, on: :collection
        post :follow, on: :member
        delete :unfollow, on: :member
        post :withdraw_money, on: :collection

        resources :tasks, only: [], controller: 'users/tasks' do
          get :created, on: :collection
          get :doing, on: :collection
        end

        resources :blocked_users, only: [:create], controller: 'users/blocked_users'
        resources :unblocked_users, only: [:destroy], controller: 'users/unblocked_users'
      end

      resources :tasks, only: [:show, :create] do
        get :approved, on: :collection
        get :closed, on: :collection
        get :created, on: :collection
        get :doing, on: :collection
        get :in_gallery, on: :collection
        put :assign_to_me, on: :member
        put :accomplish, on: :member
        post :like, on: :member
        delete :unlike, on: :member
        post :add_to_gallery, on: :member
        delete :remove_from_gallery, on: :member

        resources :comments, only: [:index, :create], controller: 'tasks/comments'
      end

      resources :chat_rooms, only: [:index] do
        get :get_chat_room, on: :collection

        resources :messages, only: [:index, :create], controller: 'chat_rooms/messages'
      end

      resources :donates, only: [:new] do
        post :create_with_card, on: :collection
        post :create_with_amount, on: :collection
      end

      resources :deposits, only: [:new, :create]
    end
  end
end
