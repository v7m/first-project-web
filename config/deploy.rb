# config valid only for current version of Capistrano
lock '3.3.5'

set :application, 'first_project'
set :full_app_name, "#{fetch(:application)}_#{fetch(:stage)}"

set :repo_url, 'git@bitbucket.org:v7m/first-project-web.git'
set :rvm_ruby_version, 'ruby-2.3.0'
server '104.131.120.163', user: 'deployer', roles: %w{web app db}

set :deploy_to, "/home/deployer/apps/#{fetch(:application)}/#{fetch(:stage)}"

set :puma_state, "#{shared_path}/tmp/pids/puma.state"

set :puma_pid, "#{shared_path}/tmp/pids/puma.pid"

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml', 'config/application.yml')

# Default value for linked_dirs is []
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 5

after 'deploy:publishing', 'deploy:restart'

namespace :deploy do
  task :restart do
    invoke 'puma:update_config'
    invoke 'puma:restart'
    invoke 'sidekiq:restart'
  end
end

namespace :puma do
  desc 'Create Directories for Puma Pids and Socket'
  task :make_dirs do
    on roles(:app) do
      execute "mkdir #{shared_path}/tmp/sockets -p"
      execute "mkdir #{shared_path}/tmp/pids -p"
    end
  end

  desc 'Update Puma config'
  task :update_config do
    on roles(:app) do
      execute :cp, "#{release_path}/config/puma/#{fetch(:stage)}.rb", "#{shared_path}/puma.rb"
    end
  end

  before :start, :make_dirs
end

namespace :redis do
  desc "Start redis-server"
  task :start do
    on roles(:app) do
      execute "redis-server --daemonize yes"
    end
  end
end