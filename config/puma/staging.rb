#!/usr/bin/env puma

directory '/home/deployer/apps/first_project/staging/current'
rackup "/home/deployer/apps/first_project/staging/current/config.ru"
environment 'staging'

pidfile "/home/deployer/apps/first_project/staging/shared/tmp/pids/puma.pid"
state_path "/home/deployer/apps/first_project/staging/shared/tmp/pids/puma.state"
stdout_redirect '/home/deployer/apps/first_project/staging/shared/log/puma_access.log', '/home/deployer/apps/first_project/staging/shared/log/puma_error.log', true

threads 0,16

bind 'unix:///home/deployer/apps/first_project/staging/shared/tmp/sockets/puma.sock'

workers 0

prune_bundler

on_restart do
  puts 'Refreshing Gemfile'
  ENV["BUNDLE_GEMFILE"] = "/home/deployer/apps/first_project/staging/current/Gemfile"
end