# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170925210605) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "blocked_users", force: :cascade do |t|
    t.integer  "user_id",         null: false
    t.integer  "blocked_user_id", null: false
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["blocked_user_id"], name: "index_blocked_users_on_blocked_user_id", using: :btree
    t.index ["user_id"], name: "index_blocked_users_on_user_id", using: :btree
  end

  create_table "chat_rooms", force: :cascade do |t|
  end

  create_table "comments", force: :cascade do |t|
    t.text     "text"
    t.integer  "user_id"
    t.integer  "task_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["task_id"], name: "index_comments_on_task_id", using: :btree
    t.index ["user_id"], name: "index_comments_on_user_id", using: :btree
  end

  create_table "deposits", force: :cascade do |t|
    t.string  "transaction_id",                                               null: false
    t.integer "financial_profile_id"
    t.decimal "amount",               precision: 8, scale: 2, default: "0.0", null: false
    t.index ["financial_profile_id"], name: "index_deposits_on_financial_profile_id", using: :btree
  end

  create_table "donates", force: :cascade do |t|
    t.string  "transaction_id",                                         null: false
    t.integer "status",                                 default: 0
    t.integer "user_id"
    t.integer "task_id"
    t.decimal "amount",         precision: 8, scale: 2, default: "0.0", null: false
    t.index ["task_id"], name: "index_donates_on_task_id", using: :btree
    t.index ["user_id"], name: "index_donates_on_user_id", using: :btree
  end

  create_table "financial_profile_replenishments", force: :cascade do |t|
    t.integer "financial_profile_id"
    t.integer "task_id"
    t.decimal "amount",               precision: 8, scale: 2, default: "0.0", null: false
    t.index ["financial_profile_id"], name: "index_financial_profile_replenishments_on_financial_profile_id", using: :btree
    t.index ["task_id"], name: "index_financial_profile_replenishments_on_task_id", using: :btree
  end

  create_table "financial_profiles", force: :cascade do |t|
    t.integer "user_id"
    t.decimal "amount",  precision: 8, scale: 2, default: "0.0", null: false
    t.index ["user_id"], name: "index_financial_profiles_on_user_id", using: :btree
  end

  create_table "follows", force: :cascade do |t|
    t.integer  "following_id", null: false
    t.integer  "follower_id",  null: false
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["follower_id"], name: "index_follows_on_follower_id", using: :btree
    t.index ["following_id", "follower_id"], name: "index_follows_on_following_id_and_follower_id", unique: true, using: :btree
    t.index ["following_id"], name: "index_follows_on_following_id", using: :btree
  end

  create_table "gallery_tasks", force: :cascade do |t|
    t.integer "user_id"
    t.integer "task_id"
    t.index ["task_id"], name: "index_gallery_tasks_on_task_id", using: :btree
    t.index ["user_id"], name: "index_gallery_tasks_on_user_id", using: :btree
  end

  create_table "likes", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "task_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["task_id"], name: "index_likes_on_task_id", using: :btree
    t.index ["user_id", "task_id"], name: "index_likes_on_user_id_and_task_id", unique: true, using: :btree
    t.index ["user_id"], name: "index_likes_on_user_id", using: :btree
  end

  create_table "messages", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "chat_room_id"
    t.text     "body",         null: false
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["chat_room_id"], name: "index_messages_on_chat_room_id", using: :btree
    t.index ["user_id"], name: "index_messages_on_user_id", using: :btree
  end

  create_table "money_withdrawals", force: :cascade do |t|
    t.integer "financial_profile_id"
    t.decimal "amount",               precision: 8, scale: 2, default: "0.0", null: false
    t.index ["financial_profile_id"], name: "index_money_withdrawals_on_financial_profile_id", using: :btree
  end

  create_table "sessions", force: :cascade do |t|
    t.string   "access_token"
    t.string   "device_token"
    t.integer  "device_platform"
    t.text     "push_token"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["access_token"], name: "index_sessions_on_access_token", unique: true, using: :btree
    t.index ["user_id"], name: "index_sessions_on_user_id", using: :btree
  end

  create_table "tasks", force: :cascade do |t|
    t.string   "title",                                                         null: false
    t.text     "description",                                                   null: false
    t.string   "video_url"
    t.integer  "user_id"
    t.datetime "created_at",                                                    null: false
    t.datetime "updated_at",                                                    null: false
    t.boolean  "accomplish",                                    default: false
    t.string   "image_url"
    t.decimal  "desired_rate"
    t.datetime "launch_at"
    t.datetime "finish_at"
    t.boolean  "doc_proof",                                     default: false
    t.boolean  "manage",                                        default: false
    t.text     "items",                                         default: [],                 array: true
    t.string   "accomplish_video_url"
    t.string   "accomplish_image_url"
    t.integer  "state",                                         default: 0
    t.integer  "doer_id"
    t.decimal  "latitude",             precision: 10, scale: 6
    t.decimal  "longitude",            precision: 10, scale: 6
    t.integer  "category",                                      default: 0
    t.string   "expiration_job_id"
    t.index ["doer_id"], name: "index_tasks_on_doer_id", using: :btree
    t.index ["user_id"], name: "index_tasks_on_user_id", using: :btree
  end

  create_table "user_chat_rooms", force: :cascade do |t|
    t.integer "user_id"
    t.integer "chat_room_id"
    t.index ["chat_room_id"], name: "index_user_chat_rooms_on_chat_room_id", using: :btree
    t.index ["user_id"], name: "index_user_chat_rooms_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "username"
    t.string   "avatar"
    t.string   "status"
    t.string   "braintree_customer_id"
    t.text     "position"
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

end
