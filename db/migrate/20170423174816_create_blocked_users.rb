class CreateBlockedUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :blocked_users do |t|
      t.integer :user_id, null: false
      t.integer :blocked_user_id, null: false

      t.timestamps null: false
    end

    add_index :blocked_users, :user_id
    add_index :blocked_users, :blocked_user_id
  end
end
