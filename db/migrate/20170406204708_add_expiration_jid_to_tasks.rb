class AddExpirationJidToTasks < ActiveRecord::Migration[5.0]
  def change
    add_column :tasks, :expiration_job_id, :string
  end
end
