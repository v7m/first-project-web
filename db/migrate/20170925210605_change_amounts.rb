class ChangeAmounts < ActiveRecord::Migration[5.0]
  def up
    remove_column :deposits, :amount
    add_column :deposits, :amount, :decimal, precision: 8, scale: 2, default: 0.0, null: false

    remove_column :donates, :amount
    add_column :donates, :amount, :decimal, precision: 8, scale: 2, default: 0.0, null: false

    remove_column :financial_profile_replenishments, :amount
    add_column :financial_profile_replenishments, :amount, :decimal, precision: 8, scale: 2, default: 0.0, null: false

    remove_column :financial_profiles, :amount
    add_column :financial_profiles, :amount, :decimal, precision: 8, scale: 2, default: 0.0, null: false

    remove_column :money_withdrawals, :amount
    add_column :money_withdrawals, :amount, :decimal, precision: 8, scale: 2, default: 0.0, null: false
  end

  def down
    remove_column :money_withdrawals, :amount
    add_column :money_withdrawals, :amount, :string, default: '0'

    remove_column :financial_profiles, :amount
    add_column :financial_profiles, :amount, :string, default: '0'

    remove_column :financial_profile_replenishments, :amount
    add_column :financial_profile_replenishments, :amount, :string, default: '0'

    remove_column :donates, :amount
    add_column :donates, :amount, :string, default: '0'

    remove_column :deposits, :amount
    add_column :deposits, :amount, :string, default: '0.0'
  end
end
