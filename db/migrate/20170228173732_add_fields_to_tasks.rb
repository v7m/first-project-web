class AddFieldsToTasks < ActiveRecord::Migration[5.0]
  def change
    add_column :tasks, :image, :string
    add_column :tasks, :desired_rate, :decimal
    add_column :tasks, :launch_at, :datetime
    add_column :tasks, :finish_at, :datetime
    add_column :tasks, :doc_proof, :boolean, default: false
    add_column :tasks, :manage, :boolean, default: false
    add_column :tasks, :approved, :boolean, default: false
  end
end
