class CreateUserChatRooms < ActiveRecord::Migration[5.0]
  def change
    create_table :user_chat_rooms do |t|
      t.belongs_to :user, index: true
      t.belongs_to :chat_room, index: true
    end
  end
end
