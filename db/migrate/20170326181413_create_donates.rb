class CreateDonates < ActiveRecord::Migration[5.0]
  def change
    create_table :donates do |t|
      t.string :transaction_id, null: false
      t.string :amount, null: false
      t.integer :status, default: 0
      t.belongs_to :user, index: true
      t.belongs_to :task, index: true
    end
  end
end
