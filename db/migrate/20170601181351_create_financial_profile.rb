class CreateFinancialProfile < ActiveRecord::Migration[5.0]
  def change
    create_table :financial_profiles do |t|
      t.string :amount, default: '0'
      t.belongs_to :user, index: true
    end
  end
end
