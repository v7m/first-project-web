class CreateFinancialProfileReplenishment < ActiveRecord::Migration[5.0]
  def change
    create_table :financial_profile_replenishments do |t|
      t.belongs_to :financial_profile, index: true
      t.belongs_to :task, index: true
      t.string :amount, default: '0'
    end
  end
end
