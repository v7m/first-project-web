class CreateSessions < ActiveRecord::Migration[5.0]
  def change
    create_table :sessions do |t|
      t.string :access_token
      t.string :device_token
      t.integer :device_platform
      t.text :push_token
      t.belongs_to :user
    end
    add_index :sessions, :access_token, unique: true
  end
end
