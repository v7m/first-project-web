class AddDoerIdToTasks < ActiveRecord::Migration[5.0]
  def change
    add_column :tasks, :doer_id, :integer
    add_index :tasks, :doer_id
  end
end
