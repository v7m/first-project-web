class ChangeTasks < ActiveRecord::Migration[5.0]
  def change
    rename_column :tasks, :video, :video_url
    rename_column :tasks, :image, :image_url
    add_column :tasks, :accomplish_video_url, :string
    add_column :tasks, :accomplish_image_url, :string
    add_column :tasks, :state, :integer, default: 0
    remove_column :tasks, :approved, :boolean, default: false
  end
end
