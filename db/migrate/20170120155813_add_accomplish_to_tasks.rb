class AddAccomplishToTasks < ActiveRecord::Migration[5.0]
  def change
    add_column :tasks, :accomplish, :boolean, default: false
  end
end
