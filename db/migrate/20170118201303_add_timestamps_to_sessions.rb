class AddTimestampsToSessions < ActiveRecord::Migration[5.0]
  def change
    add_column :sessions, :created_at, :datetime
    add_column :sessions, :updated_at, :datetime
  end
end
