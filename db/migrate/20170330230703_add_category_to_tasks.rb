class AddCategoryToTasks < ActiveRecord::Migration[5.0]
  def change
    add_column :tasks, :category, :integer, default: 0
  end
end
