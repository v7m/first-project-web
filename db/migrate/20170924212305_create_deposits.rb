class CreateDeposits < ActiveRecord::Migration[5.0]
  def change
    create_table :deposits do |t|
      t.string :amount, default: '0.0'
      t.string :transaction_id, null: false
      t.belongs_to :financial_profile, index: true
      t.timestamp
    end
  end
end
