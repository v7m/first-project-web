class CreateMoneyWithdrawals < ActiveRecord::Migration[5.0]
  def change
    create_table :money_withdrawals do |t|
      t.belongs_to :financial_profile, index: true
      t.string :amount, default: '0', null: false
    end
  end
end
