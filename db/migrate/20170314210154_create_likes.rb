class CreateLikes < ActiveRecord::Migration[5.0]
  def change
    create_table :likes do |t|
      t.belongs_to :user, index: true
      t.belongs_to :task, index: true

      t.timestamps null: false
    end

    add_index :likes, [:user_id, :task_id], unique: true
  end
end
