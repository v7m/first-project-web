class ChangeTextAtComments < ActiveRecord::Migration[5.0]
  def up
    change_column :comments, :text, :text
  end

  def down
    change_column :comments, :text, :string
  end
end
