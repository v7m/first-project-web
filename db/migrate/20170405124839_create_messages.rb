class CreateMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :messages do |t|
      t.belongs_to :user, index: true
      t.belongs_to :chat_room, index: true
      t.text :body, null: false
    end
  end
end
